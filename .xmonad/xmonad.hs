{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

import XMonad
import XMonad.Config.Desktop

-- Utilities
import XMonad.Util.EZConfig (additionalKeysP)
--import XMonad.Util.NamedScratchpad
--import XMonad.Util.Run (safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce

-- XMonad Prompts
import XMonad.Prompt
--import XMonad.Prompt.Man
--import XMonad.Prompt.Pass
--import XMonad.Prompt.Shell (shellPrompt)
--import XMonad.Prompt.Ssh
import XMonad.Prompt.XMonad
--import Control.Arrow (first)

-- Hooks
import XMonad.Hooks.SetWMName
import XMonad.Hooks.EwmhDesktops
--import XMonad.Hooks.ManageDocks

-- Actions
import XMonad.Actions.GridSelect

-- System
import System.Exit (exitSuccess)

-- Data
--import qualified Data.Map as M
import Data.List

-- Layout Management
--import XMonad.Layout.Gaps


-- Taffybar
-- import System.Taffybar.Support.PagerHints (pagerHints)

------------------------------------------------------------------------
-- AUTOSTART
------------------------------------------------------------------------
--

myStartupHook = do
          spawnOnce "taffybar &"
          spawnOnce "nitrogen --restore &" 
          spawnOnce "nm-applet &"
          spawnOnce "volumeicon &"
--          spawnOnce "trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 0 --tint 0x292d3e --height 18 &"
          spawnOnce "$HOME/.xmonad/my_xsl.sh"
          setWMName "LG3D"


------------------------------------------------------------------------
-- CONSTANTS
------------------------------------------------------------------------
--

myFont :: [Char]
myFont = "xft:FantasqueSansMono Nerd Font:12"

myTerminal :: [Char]
myTerminal = "/home/hutch/.local/bin/st"

myBorderWidth :: Dimension
myBorderWidth = 2

supMask :: KeyMask
supMask = mod4Mask

--altMask :: KeyMask
--altMask = mod1Mask

------------------------------------------------------------------------
-- GRID SELECT
------------------------------------------------------------------------
myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName
                  (0x31,0x2e,0x39) -- lowest inactive bg
                  (0x31,0x2e,0x39) -- highest inactive bg
                  (0x61,0x57,0x72) -- active bg
                  (0xc0,0xa7,0x9a) -- inactive fg
                  (0xff,0xff,0xff) -- active fg
 
-- gridSelect menu layout
-- myGridConfig :: p -> GSConfig Window
myGridConfig colorizer = (buildDefaultGSConfig colorizer)
    { gs_cellheight   = 30
    , gs_cellwidth    = 200
    , gs_cellpadding  = 8
    , gs_originFractX = 0.5
    , gs_originFractY = 0.5
--    , gs_font         = myFont
    }
spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
    where conf = def

appGrid :: [(String, String)]
appGrid = [ ("Firefox",   "$HOME/.local/bin/firefox")
          , ("Xournal++", "xournalpp")
          , ("KeePassX",  "keepassx")
          , ("Signal",    "signal-desktop")
          ]

exitGridOptions :: [(String, X ())]
exitGridOptions =  [ ("Reboot",     spawn "reboot")
            , ("Power Off",  spawn "shutdown now")
            , ("Suspend to RAM", spawn "systemctl suspend")
            , ("Exit to DM", io exitSuccess)
            , ("Lock Screen", spawn "xset s activate")
            ]

exitGrid :: X ()
exitGrid = runSelectedAction conf exitGridOptions
    where conf = def

------------------------------------------------------------------------
-- XPROMPT SETTINGS
------------------------------------------------------------------------
myXMPromptConfig :: XPConfig
myXMPromptConfig = def
      { font                = myFont
      , bgColor             = "#292d3e"
      , fgColor             = "#d0d0d0"
      , bgHLight            = "#c792ea"
      , fgHLight            = "#000000"
      , borderColor         = "#535974"
      , promptBorderWidth   = 2
      -- , promptKeymap        = dtXPKeymap
      , position            = Top
--    , position            = CenteredAt { xpCenterY = 0.3, xpWidth = 0.3 }
      , height              = 20
      , historySize         = 256
      , historyFilter       = id
      , defaultText         = []
      , autoComplete        = Just 100000    -- set Just 100000 for .1 sec
      , showCompletionOnTab = False
      , searchPredicate     = isPrefixOf
      , alwaysHighlight     = True
      , maxComplRows        = Nothing        -- set to Just 5 for 5 rows
      }



------------------------------------------------------------------------
-- KEYBINDS
------------------------------------------------------------------------
--

myKeyBinds :: [([Char] , X())]
myKeyBinds =
        [
        -- WM-level actions
          ("M-C-r", spawn "xmonad --recompile")
        , ("M-S-r", spawn "xmonad --restart")
        , ("M-S-q", exitGrid)
        , ("<XF86ScreenSaver>", exitGrid)
        -- Prompt
        , ("M-S-o", xmonadPrompt myXMPromptConfig)
        , ("M-d", spawn "rofi -show run")
        , ("M-z", spawnSelected' appGrid)
        , ("M-S-g", goToSelected $ myGridConfig myColorizer)
        , ("M-S-b", bringSelected $ myGridConfig myColorizer)
        ]


------------------------------------------------------------------------
-- MAIN
------------------------------------------------------------------------
--



main :: IO ()
main = xmonad $
--        docks $
       ewmh $
--        pagerHints
       desktopConfig
           { terminal = myTerminal
           , modMask  = supMask
           , startupHook = myStartupHook
           , borderWidth = myBorderWidth
           } `additionalKeysP` myKeyBinds

