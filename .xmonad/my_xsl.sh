#! /bin/sh
xset 180 5

xss-lock \
    -n /usr/local/libexec/xsecurelock/dimmer \
    -l -- \
env \
XSECURELOCK_SAVER=saver_xscreensaver \
XSECURELOCK_AUTHPROTO=authproto_pam  \
XSECURELOCK_PAM_SERVICE=xsecurelock  \
XSECURELOCK_PASSWORD_PROMPT=kaomoji  \
XSECURELOCK_FONT="FantasqueSansMono Nerd Font" \
xsecurelock

