set number


set nocompatible              " required
filetype off                  " required

" set runtimepath^=~/.vim runtimepath+=~.vim/after
" let &packpath = &runtimepath
"

" set python interpreter (this one lives in a spack env on cicero)
let g:python3_host_prog = '/home/hutch/.local/spack/var/spack/environments/misc_python38/.spack-env/view/bin/python'

source ~/.vimrc
