# $HOME/.config/i3/toggletouchpad.sh
#! /bin/bash

if xinput list-props "ETPS/2 Elantech Touchpad" | grep "Device Enabled.*0" ; then
    xinput set-prop  "ETPS/2 Elantech Touchpad" "Device Enabled" 1 ;
else
    xinput set-prop  "ETPS/2 Elantech Touchpad" "Device Enabled" 0 ;
fi

