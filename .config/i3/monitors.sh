#! /bin/sh

INTERNAL_MONITOR="eDP-1"
EXTERNAL_MONITOR="HDMI-1"

pkill polybar;

echo $INTERNAL_MONITOR "is the internal monitor"

if [ $1 = "internal" ]; then
    xrandr --output $INTERNAL_MONITOR --auto --output $EXTERNAL_MONITOR --off;
    polybar $INTERNAL_MONITOR >/dev/null 2>/dev/null &
fi

if [ $1 = "external" ]; then
    xrandr --output $INTERNAL_MONITOR --off --output $EXTERNAL_MONITOR --auto;
    polybar $EXTERNAL_MONITOR >/dev/null 2>/dev/null &
fi

if [ $1 = "mirror" ]; then
    xrandr --output $INTERNAL_MONITOR --auto --output $EXTERNAL_MONITOR --auto --same-as $INTERNAL_MONITOR;
    polybar $INTERNAL_MONITOR >/dev/null 2>/dev/null &
fi

if [ $1 = "extend" ]; then
    xrandr --output $INTERNAL_MONITOR --auto --output $EXTERNAL_MONITOR --auto --right-of $INTERNAL_MONITOR;
    polybar $INTERNAL_MONITOR >/dev/null 2>/dev/null &
    polybar $EXTERNAL_MONITOR >/dev/null 2>/dev/null &
fi

