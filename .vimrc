set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
" set rtp+=~/.vim/bundle/Vundle.vim
call plug#begin('~/.vim/plugged')

" let Vundle manage Vundle, required
" Plug 'gmarik/Vundle.vim'

" Add all your plugins here (note older versions of Vundle used Bundle instead of Plug)

" Indent fold assistant
Plug 'tmhedberg/SimpylFold'

" Git command wrapper
" Plug 'tpope/vim-fugitive'

" Syntax Highlighting Assistants
" Plug 'vim-syntastic/syntastic'
" Plug 'nvie/vim-flake8'

" deoplete (completions)
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
let g:deoplete#enable_at_startup = 1
Plug 'deoplete-plugins/deoplete-jedi'
Plug 'Shougo/neco-syntax'

" Filetree
Plug 'scrooloose/nerdtree'

" Colschemes
Plug 'jnurmine/Zenburn'
Plug 'altercation/vim-colors-solarized'

" TMUX-aware panel switching
Plug 'christoomey/vim-tmux-navigator'

" More imformation in status-line
" Plug 'Lokaltog/powerline'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Rust Language configuration
Plug 'rust-lang/rust.vim'

" Vim-tmux command line interaction
Plug 'benmills/vimux'

" Haskell
Plug 'dag/vim2hs'
Plug 'eagletmt/neco-ghc'

" LaTeX
Plug 'vim-latex/vim-latex'
Plug 'ying17zi/vim-live-latex-preview'

" fzf - fuzzy finder
Plug 'junegunn/fzf.vim'

" Surround - make quotes/brackets surround a selection
Plug 'tpope/vim-surround'

" gitgutter - git status overview in vim
Plug 'airblade/vim-gitgutter'

" ALE - the Asynchronous Linting Engine
Plug 'w0rp/ale'

" Gruvbox theme
Plug 'morhetz/gruvbox'


" All of your Plugins must be added before the following line
call plug#end()
filetype plugin indent on    " required

" Automatically load plugins and collapse PluginInstall frame at startup
autocmd vimenter * PlugInstall
autocmd vimenter * q

" Allow PowerLine to use fancy symbols
" let g:Powerline_symbols = 'fancy'

" Set vim-airline theming
let g:airline_theme='gruvbox'
let g:airline#extensions#tabline#enabled = 1



" Ensure that frame-splits are handled in a perdictable way
set splitbelow
set splitright

" Parencetera matching
set showmatch
set mat=2

"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Enable folding
set foldmethod=indent
set foldlevel=99

" Enable folding with the spacebar
nnoremap <space> za

set encoding=utf-8

" Turn on Syntax Highlighting
let python_highlight_all=1
syntax on

" Turn on Line Numbering
set nu

" Colorscheme control
set t_Co=256
if has('gui_running')
    set background=light
    colorscheme solarized
else
    set background=dark
    colorscheme gruvbox
endif

" Allow advanced mouse controls
set mouse=a

" Spaces on tab key
set tabstop=4
set expandtab
set shiftwidth=4


"Autostart Filetree, autoquit if only filetree left
autocmd vimenter * NERDTree
autocmd vimenter * vertical resize 15
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") && system("tput cols") > 50 | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Hotkey to toggle filetree
map <F5> :NERDTreeToggle<CR>

" Add keybinds for Vimux
cmap vp VimuxPrompt<CR>

" Spellchecker, mapped to F6
map <F6> :setlocal spell! spelllang=en_us<CR>

" system clipboard default
set clipboard=unnamedplus


